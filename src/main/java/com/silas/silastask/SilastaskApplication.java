package com.silas.silastask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SilastaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(SilastaskApplication.class, args);
	}

}
