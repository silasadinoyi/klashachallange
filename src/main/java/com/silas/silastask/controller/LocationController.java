package com.silas.silastask.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.silas.silastask.data.*;
import com.silas.silastask.dto.request.CertainDetailsDto;
import com.silas.silastask.dto.request.CityDto;
import com.silas.silastask.dto.request.CountryStatesCitiesDto;
import com.silas.silastask.dto.request.ForeignExchange;
import com.silas.silastask.service.LocationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
@Slf4j
public class LocationController {
    private final LocationService locationService;

    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }

    // Question 1
    @GetMapping("/popular-cities")
    public List<CityDto> mostPopulatedCities(@RequestParam int numberOfCities) throws JsonProcessingException {
        return locationService.getMostPopulatedCitiesFromSelectedCountries(numberOfCities);
    }
    // Question 2
    @GetMapping("/certain-details")
    public CertainDetailsDto getCertainDetails(@RequestParam String country)  {
        return locationService.fetchCertainDetailsFromACountry(country);
    }
    // Question 3
    @GetMapping("/country-states-cities")
    public CountryStatesCitiesDto getCountryStatesCities(@RequestParam String country)  {
        return locationService.countryStatesAndCities(country);
    }
    // Question 4
    @GetMapping("/foreign-exchange")
    public ForeignExchange foreignExchangeFormat(@RequestParam String country,
                                                 @RequestParam int monetaryAmount,
                                                 @RequestParam String targetCurrency)  {
        return locationService.foreignExchangeFormat(country,monetaryAmount,targetCurrency);
    }



    /******************** Helper Endpoints **********************/

//    @GetMapping("/country-population")
//    public CountryPopulationData getCountryPopulation(@RequestParam String country){
//        return locationService.getCountryPopulation(country);
//    }

//    @GetMapping("/country-capital")
//    public CountryCapitalData getCountryCapital(@RequestParam String country){
//        return locationService.getCountryCapital(country);
//    }
//
//    @GetMapping("/country-location")
//    public CountryLocationData getCountryLocation(@RequestParam String country){
//        return locationService.getCountryLocation(country);
//    }

//    @GetMapping("/country-currency")
//    public CountryCurrencyData getCountryCurrency(@RequestParam String country){
//        return locationService.getCountryCurrency(country);
//    }

//    @GetMapping("/country-isocodes")
//    public CountryIsoCodesData getCountryIsoCodes(@RequestParam String country){
//        return locationService.getCountryIsoCodes(country);
//    }

//    @GetMapping("/country-states")
//    public StatesInCountryData getCountryStates(@RequestParam String country){
//        return locationService.getAllStatesInCountry(country);
//    }

//    @GetMapping("/country-states-cities")
//    public List<String> getCountryStatesCities(@RequestParam String country, @RequestParam String state){
//        log.info("GOT TO THE CONTROLLER");
//        return locationService.getAllCitiesInState(country,state);
//    }

}
