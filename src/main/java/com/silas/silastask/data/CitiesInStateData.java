package com.silas.silastask.data;

import lombok.Data;

import java.util.List;

@Data
public class CitiesInStateData {
    private List<String> cities;
}
