package com.silas.silastask.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CityData {
    private String country;
    private String city;
    private List<CityPopulationData> populationCounts;
}
