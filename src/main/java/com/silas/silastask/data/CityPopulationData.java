package com.silas.silastask.data;

import lombok.Data;

@Data
public class CityPopulationData {
    private String year;
    private String value;
    private String sex;
    private String reliabilty;
}
