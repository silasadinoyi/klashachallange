package com.silas.silastask.data;

import lombok.Data;

@Data
public class CountryCapitalData {
    private String name;
    private String capital;
    private String iso2;
    private String iso3;
}
