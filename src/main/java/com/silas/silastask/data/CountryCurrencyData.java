package com.silas.silastask.data;

import lombok.Data;

@Data
public class CountryCurrencyData {
    private String name;
    private String currency;
    private String iso2;
    private String iso3;
}
