package com.silas.silastask.data;

import lombok.Data;

@Data
public class CountryIsoCodesData {
    private String name;
    private String Iso2;
    private String Iso3;
}
