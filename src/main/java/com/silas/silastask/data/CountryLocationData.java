package com.silas.silastask.data;

import lombok.Data;

@Data
public class CountryLocationData {
    private String name;
    private String iso2;
    private int lng;
    private int lat;
}
