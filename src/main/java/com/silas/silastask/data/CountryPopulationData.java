package com.silas.silastask.data;

import lombok.Data;

import java.util.List;

@Data
public class CountryPopulationData {
    private String country;
    private String code;
    private String iso3;
    private List<PopulationData> populationCounts;
}
