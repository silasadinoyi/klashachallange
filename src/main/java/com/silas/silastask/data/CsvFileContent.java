package com.silas.silastask.data;

import lombok.Data;

@Data
public class CsvFileContent {
    private String sourceCurrency;
    private String targetCurrency;
    private String rate;
    private String lastColumn;
}
