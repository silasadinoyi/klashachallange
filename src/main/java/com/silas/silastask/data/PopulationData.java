package com.silas.silastask.data;

import lombok.Data;

@Data
public class PopulationData {
    private int year;
    private int value;
}
