package com.silas.silastask.data;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PositionData {
    private int lng;
    private int lat;
}
