package com.silas.silastask.data;

import lombok.Data;

@Data
public class StateData {
    private String name;
    private String state_code;
}
