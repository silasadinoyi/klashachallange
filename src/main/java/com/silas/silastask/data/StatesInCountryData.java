package com.silas.silastask.data;

import lombok.Data;

import java.util.List;

@Data
public class StatesInCountryData {
    private String name;
    private String iso3;
    private List<StateData> states;
}
