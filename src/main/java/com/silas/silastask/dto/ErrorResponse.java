package com.silas.silastask.dto;


public class ErrorResponse {

    private String responseMessage;
    private int responseCode;


    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    @Override
    public String toString() {
        return "ErrorResponse{" +
                "responseMessage='" + responseMessage + '\'' +
                ", responseCode=" + responseCode +
                '}';
    }
}
