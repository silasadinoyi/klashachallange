package com.silas.silastask.dto;

import lombok.Data;

@Data
public class HeaderBase {
    private Boolean error;
    private String msg;
}
