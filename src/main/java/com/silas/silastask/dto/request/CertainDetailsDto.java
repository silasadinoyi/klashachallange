package com.silas.silastask.dto.request;

import com.silas.silastask.data.PopulationData;
import com.silas.silastask.data.PositionData;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CertainDetailsDto {
    private PositionData location;
    private String capital;
    private String currency;
    private String iso2;
    private String iso3;
    private List<PopulationData> population;
}
