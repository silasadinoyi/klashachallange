package com.silas.silastask.dto.request;

import com.silas.silastask.dto.HeaderBase;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ForeignExchange {
    private String countryCurrency;
    private Double convertedValue;

}
