package com.silas.silastask.dto.response;

import com.silas.silastask.dto.HeaderBase;
import lombok.Data;

import java.util.List;

@Data
public class CitiesInStateDataResponse {
    private HeaderBase headerBase;
    private List<String> data;
}
