package com.silas.silastask.dto.response;

import com.silas.silastask.data.CityData;
import com.silas.silastask.dto.HeaderBase;
import lombok.Data;

import java.util.List;

@Data
public class FilterCityAndPopulationDataResponse {
    private HeaderBase headerBase;
    private List<CityData> data;
}
