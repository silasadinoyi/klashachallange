package com.silas.silastask.dto.response;

import com.silas.silastask.data.CountryCapitalData;
import com.silas.silastask.dto.HeaderBase;
import lombok.Data;

@Data
public class SingleCountryAndCapitalDataResponse {
    private HeaderBase headerBase;
    private CountryCapitalData data;
}
