package com.silas.silastask.dto.response;

import com.silas.silastask.data.CountryCurrencyData;
import com.silas.silastask.dto.HeaderBase;
import lombok.Data;

@Data
public class SingleCountryAndCurrencyDataResponse {
    private HeaderBase headerBase;
    private CountryCurrencyData data;
}
