package com.silas.silastask.dto.response;

import com.silas.silastask.data.CountryPopulationData;
import com.silas.silastask.dto.HeaderBase;
import lombok.Data;

@Data
public class SingleCountryAndPopulationDataResponse {
    private HeaderBase headerBase;
    private CountryPopulationData data;
}
