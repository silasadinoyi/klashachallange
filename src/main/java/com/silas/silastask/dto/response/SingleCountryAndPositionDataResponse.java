package com.silas.silastask.dto.response;

import com.silas.silastask.data.CountryLocationData;
import com.silas.silastask.dto.HeaderBase;
import lombok.Data;

@Data
public class SingleCountryAndPositionDataResponse {
    private HeaderBase headerBase;
    private CountryLocationData data;
}
