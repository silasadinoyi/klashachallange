package com.silas.silastask.dto.response;

import com.silas.silastask.data.StatesInCountryData;
import com.silas.silastask.dto.HeaderBase;
import lombok.Data;

@Data
public class SingleCountryAndStates {
    private HeaderBase headerBase;
    private StatesInCountryData data;
}
