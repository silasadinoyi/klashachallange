package com.silas.silastask.dto.response;

import com.silas.silastask.data.CountryIsoCodesData;
import com.silas.silastask.dto.HeaderBase;
import lombok.Data;

@Data
public class SingleCountryIsoCodesDataResponse {
    private HeaderBase headerBase;
    private CountryIsoCodesData data;
}
