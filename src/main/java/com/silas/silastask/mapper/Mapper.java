package com.silas.silastask.mapper;

import com.silas.silastask.data.*;
import com.silas.silastask.dto.request.CertainDetailsDto;
import com.silas.silastask.dto.request.CityDto;
import com.silas.silastask.dto.request.CountryStatesCitiesDto;
import com.silas.silastask.dto.request.StateDto;
import com.silas.silastask.utils.JsonConverter;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class Mapper {

    public static CityDto mapToCountryData(CityData cityData) {

        return CityDto.builder()
                .country(cityData.getCountry())
                .city(cityData.getCity())
                .population(cityData.getPopulationCounts().get(0).getValue())
                .build();
    }

    public static CertainDetailsDto mapToCertainDetails(
            CountryPopulationData countryPopulationData,
            CountryCapitalData countryCapitalData,
            CountryLocationData countryLocationData,
            CountryCurrencyData countryCurrencyData
            ){
        return CertainDetailsDto.builder()
                .population(countryPopulationData.getPopulationCounts())
                .capital(countryCapitalData.getCapital())
                .location(mapToPositionData(countryLocationData))
                .currency(countryCurrencyData.getCurrency())
                .iso2(countryCapitalData.getIso2())
                .iso3(countryCapitalData.getIso3())
                .build();
    }

    public static PositionData mapToPositionData(CountryLocationData countryLocationData){
        return PositionData.builder()
                .lat(countryLocationData.getLat())
                .lng(countryLocationData.getLng())
                .build();
    }

    public static CountryStatesCitiesDto mapToCountryStateCitiesDto(
            StatesInCountryData allStatesInCountry,
            List<StateDto> allStates
            ){

        return CountryStatesCitiesDto.builder()
                .countryName(allStatesInCountry.getName())
                .states(allStates)
                .build();
    }

}
