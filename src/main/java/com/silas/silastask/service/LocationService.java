package com.silas.silastask.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.silas.silastask.data.*;
import com.silas.silastask.dto.request.CertainDetailsDto;
import com.silas.silastask.dto.request.CityDto;
import com.silas.silastask.dto.request.CountryStatesCitiesDto;
import com.silas.silastask.dto.request.ForeignExchange;

import java.util.List;

public interface LocationService {

    List<CityDto> getMostPopulatedCitiesFromSelectedCountries(int numberOfCities) throws JsonProcessingException;

    CertainDetailsDto fetchCertainDetailsFromACountry(String country);

    CountryStatesCitiesDto countryStatesAndCities(String country);

    ForeignExchange foreignExchangeFormat(String country, int monetaryAmount, String targetCurrency);

    List<CityData> getCitiesInCountry(String country);

    CountryPopulationData getCountryPopulation(String country);

    CountryCapitalData getCountryCapital(String country);

    CountryLocationData getCountryLocation(String country);

    CountryCurrencyData getCountryCurrency(String country);

//    CountryIsoCodesData getCountryIsoCodes(String country);

    StatesInCountryData getAllStatesInCountry(String country);

    List<String> getAllCitiesInState(String country,String state);

}
