package com.silas.silastask.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.silas.silastask.data.*;
import com.silas.silastask.dto.ErrorResponse;
import com.silas.silastask.dto.request.*;
import com.silas.silastask.dto.response.*;
import com.silas.silastask.mapper.Mapper;
import com.silas.silastask.utils.CurrencyConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.*;
import java.util.logging.Logger;
import java.util.stream.Stream;

@Service
@Slf4j
public class LocationServiceImpl implements LocationService{
    private final RestTemplate restTemplate;

    public LocationServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    private static final Logger logger = Logger.getLogger(LocationServiceImpl.class.getName());

    @Value("${getCitiesInCountryApiUrl}")
    private String getCitiesInCountryApiUrl;

    @Value("${getCountryPopulationApiUrl}")
    private String getCountryPopulationApiUrl;

    @Value("${getCountryCapitalApiUrl}")
    private String getCountryCapitalApiUrl;

    @Value("${getCountryLocationApiUrl}")
    private String getCountryLocationApiUrl;

    @Value("${getCountryCurrencyApiUrl}")
    private String getCountryCurrencyApiUrl;

    @Value("${getAllStatesInCountryApiUrl}")
    private String getAllStatesInCountryApiUrl;

    @Value("${getAllCitiesInStateApiUrl}")
    private String getAllCitiesInStateApiUrl;

    // Question 1
    @Override
    public List<CityDto> getMostPopulatedCitiesFromSelectedCountries(int numberOfCities) throws JsonProcessingException {
        List<CityData> italy = getCitiesInCountry("Italy");
        List<CityData> ghana = getCitiesInCountry("Ghana");
        List<CityData> newZealand = getCitiesInCountry("New Zealand");

        List<CityData> cities = Stream.of(italy, ghana, newZealand).flatMap(List::stream).toList();

       return cities.stream().sorted((first,second)->{
            int value1 = Integer.valueOf(first.getPopulationCounts().get(0).getValue());
            int value2 = Integer.valueOf(second.getPopulationCounts().get(0).getValue());
            return Integer.compare(value2,value1);
        }).limit(numberOfCities).map(Mapper::mapToCountryData).toList();

    }

    // Question 2
    @Override
    public CertainDetailsDto fetchCertainDetailsFromACountry(String country) {
        // Create instance of objects
        CountryPopulationData countryPopulationData = new CountryPopulationData();
        CountryCapitalData countryCapitalData = new CountryCapitalData();
        CountryLocationData countryLocationData =  new CountryLocationData();
        CountryCurrencyData countryCurrencyData = new CountryCurrencyData();

        // Create executable service
        ExecutorService executorService = Executors.newFixedThreadPool(4);

        // Define callable tasks
        Callable<CountryPopulationData> countryPopulationTask = ()-> getCountryPopulation(country);
        Callable<CountryCapitalData> countryCapitalTask = ()-> getCountryCapital(country);
        Callable<CountryLocationData> countryLocationTask = ()-> getCountryLocation(country);
        Callable<CountryCurrencyData> countryCurrencyTask = ()-> getCountryCurrency(country);

        try {
            // Submit callable task to get a future object
            Future<CountryPopulationData> countryPopulationDataFuture = executorService.submit(countryPopulationTask);
            Future<CountryCapitalData> countryCapitalDataFuture = executorService.submit(countryCapitalTask);
            Future<CountryLocationData> countryLocationDataFuture = executorService.submit(countryLocationTask);
            Future<CountryCurrencyData> countryCurrencyDataFuture = executorService.submit(countryCurrencyTask);

            // Using a timeout of 10 seconds
            long timeout = 10;
            TimeUnit timeUnit = TimeUnit.SECONDS;

            // Get the result from the callable task
            countryPopulationData = countryPopulationDataFuture.get(timeout, timeUnit);
            countryCapitalData = countryCapitalDataFuture.get(timeout, timeUnit);
            countryLocationData = countryLocationDataFuture.get(timeout, timeUnit);
            countryCurrencyData = countryCurrencyDataFuture.get(timeout, timeUnit);

        }catch (InterruptedException | ExecutionException | TimeoutException e){
            e.printStackTrace();
            logger.severe("Error fetching details: " + e.getMessage());
        }finally {
            executorService.shutdown();
        }
        return Mapper.mapToCertainDetails(countryPopulationData, countryCapitalData, countryLocationData, countryCurrencyData);
    }

    // Question 3
    @Override
    public CountryStatesCitiesDto countryStatesAndCities(String country) {
        StatesInCountryData allStatesInCountry = new StatesInCountryData();
        List<StateDto> allStates = new ArrayList<>();
        ErrorResponse resp = new ErrorResponse();
        StateDto stateDto = new StateDto();
        try {
            allStatesInCountry = getAllStatesInCountry(country);
            List<StateData> states = allStatesInCountry.getStates();


            for(StateData stateObj : states){
                List<String> allCitiesInState = getAllCitiesInState(country, stateObj.getName());
                stateDto = StateDto.builder()
                        .name(stateObj.getName())
                        .state_code(stateObj.getState_code())
                        .cities(allCitiesInState)
                        .build();

                allStates.add(stateDto);
            }
        } catch (Exception e) {
//            log.info(e.printStackTrace());

            resp.setResponseCode(500);
            resp.setResponseMessage(e.getMessage());

//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);
        }

        return Mapper.mapToCountryStateCitiesDto(allStatesInCountry,allStates);
    }

    // Question 4
    // Try using RequestBody also
    @Override
    public ForeignExchange foreignExchangeFormat(String country, int monetaryAmount, String targetCurrency) {

        CountryCurrencyData countryCurrency = getCountryCurrency(country);
        Double converted = CurrencyConverter.convertCurrency(countryCurrency.getCurrency(), monetaryAmount, targetCurrency);

//        log.info("Converted Message->{}",CurrencyConverter.msg);

        return ForeignExchange.builder()
                .countryCurrency(countryCurrency.getCurrency())
                .convertedValue(converted)
                .build();
    }


    /********************************* HELPER METHODS ***********************************/

    // QUESTION 1 HELPER METHOD
    @Override
    public List<CityData> getCitiesInCountry(String country) {
        String apiUrl = getCitiesInCountryApiUrl + "&country=" + country;

       try {
           ResponseEntity<FilterCityAndPopulationDataResponse> dataResponse = restTemplate.getForEntity(
                   apiUrl, FilterCityAndPopulationDataResponse.class);
           return Objects.requireNonNull(dataResponse.getBody()).getData();

       }catch(RestClientException e){
           logger.severe("Error fetching cities data in country for " + country + ": " + e.getMessage());
           return null;
       }
    }

    // QUESTION 2 HELPER METHOD
    @Override
    public CountryPopulationData getCountryPopulation(String country) {

        String apiUrl = getCountryPopulationApiUrl + "&country=" + country;

        try {
            ResponseEntity<SingleCountryAndPopulationDataResponse> dataResponse = restTemplate.getForEntity(
                    apiUrl, SingleCountryAndPopulationDataResponse.class);
            return Objects.requireNonNull(dataResponse.getBody()).getData();
        }catch (RestClientException e){
            logger.severe("Error fetching country population data for " + country + ": " + e.getMessage());
            return null;
        }
    }

    // QUESTION 2 HELPER METHOD
    @Override
    public CountryCapitalData getCountryCapital(String country) {
        String apiUrl = getCountryCapitalApiUrl + "&country=" + country;

      try {
          ResponseEntity<SingleCountryAndCapitalDataResponse> dataResponse = restTemplate.getForEntity(
                  apiUrl, SingleCountryAndCapitalDataResponse.class);

          return Objects.requireNonNull(dataResponse.getBody()).getData();
      }catch (RestClientException e){
          logger.severe("Error fetching country CAPITAL data for " + country + ": " + e.getMessage());
          return null;
      }
    }


    // QUESTION 2 HELPER METHOD
    @Override
    public CountryLocationData getCountryLocation(String country) {
        String apiUrl = getCountryLocationApiUrl + "&country=" + country;

        try {
            ResponseEntity<SingleCountryAndPositionDataResponse> dataResponse = restTemplate.getForEntity(apiUrl, SingleCountryAndPositionDataResponse.class);

            return Objects.requireNonNull(dataResponse.getBody()).getData();
        }catch (RestClientException e){
            logger.severe("Error fetching country location data for " + country + ": " + e.getMessage());
            return null;
        }
    }

    // QUESTION 2 HELPER METHOD
    @Override
    public CountryCurrencyData getCountryCurrency(String country) {
        String apiUrl = getCountryCurrencyApiUrl + "&country=" + country;

        try {
            ResponseEntity<SingleCountryAndCurrencyDataResponse> dataResponse = restTemplate.getForEntity(apiUrl, SingleCountryAndCurrencyDataResponse.class);

            return Objects.requireNonNull(dataResponse.getBody()).getData();
        }catch (RestClientException e){
            logger.severe("Error fetching country currency data for " + country + ": " + e.getMessage());
            return null;
        }
    }

//    // QUESTION 2 HELPER METHOD
//    @Override
//    public CountryIsoCodesData getCountryIsoCodes(String country) {
//        String apiUrl = "https://countriesnow.space/api/v0.1/countries/iso/q?" + "&country=" + country;
//
//        ResponseEntity<SingleCountryIsoCodesDataResponse> dataResponse = restTemplate.getForEntity(apiUrl, SingleCountryIsoCodesDataResponse.class);
//
//        return Objects.requireNonNull(dataResponse.getBody()).getData();
//    }

    // QUESTION 3 HELPER METHOD
    @Override
    public StatesInCountryData getAllStatesInCountry(String country) {
        String apiUrl = getAllStatesInCountryApiUrl + "&country=" + country;

        try {
            ResponseEntity<SingleCountryAndStates> dataResponse = restTemplate.getForEntity(
                    apiUrl, SingleCountryAndStates.class);
            return Objects.requireNonNull(dataResponse.getBody()).getData();
        }catch (RestClientException e){
            logger.severe("Error fetching states data in country for " + country + ": " + e.getMessage());
            return null;
        }
    }

    // QUESTION 3 HELPER METHOD
    @Override
    public List<String> getAllCitiesInState(String country,String state) {

        String apiUrl = getAllCitiesInStateApiUrl + "&country=" + country + "&state=" + state;

        try {
            ResponseEntity<CitiesInStateDataResponse> dataResponse = restTemplate.getForEntity(
                    apiUrl, CitiesInStateDataResponse.class);
            return Objects.requireNonNull(dataResponse.getBody()).getData();
        }catch (RestClientException e){
            logger.severe("Error fetching cities in state for " + state + ": " + e.getMessage());
            return null;
        }
    }


}