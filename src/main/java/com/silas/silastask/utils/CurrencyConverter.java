package com.silas.silastask.utils;

import com.silas.silastask.data.CsvFileContent;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Objects;

@Slf4j
public class CurrencyConverter {
    public static String msg="";
    public static Double convertCurrency(String currency, int amount, String targetCurrency) {

        List<CsvFileContent> csvFileContents = FileReaderManager.readCsvUsingBufferReader(
                "src/main/resources/exchange_rate (1).csv");

        double converted = 0.0;

        for (CsvFileContent content : csvFileContents) {

            if (Objects.equals(content.getSourceCurrency(), currency) && Objects.equals(content.getTargetCurrency(), targetCurrency)) {
                converted = amount * Double.parseDouble(content.getRate());
                msg="Successfully converted" + content.getSourceCurrency() + amount + " to " + content.getTargetCurrency() + converted;
//                double result = amount * Double.parseDouble(content.getRate());
//                log.info("Result:{}", result);
            }

            if (Objects.equals(content.getSourceCurrency(), targetCurrency) && Objects.equals(content.getTargetCurrency(), currency)) {
                converted = amount / Double.parseDouble(content.getRate());
                msg="Successfully converted" + content.getSourceCurrency() + amount + " to " + content.getTargetCurrency() + converted;
//                double result = amount / Double.parseDouble(content.getRate());
//                log.info("Result:{}", result);
            }

//            if (!(Objects.equals(content.getSourceCurrency(), currency) && Objects.equals(content.getTargetCurrency(), targetCurrency))) {
////                msg="Invalid currency!. Currency not found";
////                log.info("msg: Invalid currency!. Currency not found");
//            }
        }
        return converted;
    }
}
