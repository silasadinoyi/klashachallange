package com.silas.silastask.utils;

import com.silas.silastask.data.CsvFileContent;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Slf4j
public class FileReaderManager {
//    public static void readCsvUsingScanner(String filePath){
//        try{
//            Scanner scanner = new Scanner(new File(filePath));
//            while (scanner.hasNext()){
//                System.out.println("Scanner->" + scanner.next().toString());
//            }
//            scanner.close();
//        }catch (FileNotFoundException e){
//            e.printStackTrace();
//        }
//
//    }

    public static List<CsvFileContent>  readCsvUsingBufferReader(String filePath){
        String line = "";
        List<CsvFileContent> contents = new ArrayList<>();
        try(BufferedReader reader = new BufferedReader(new java.io.FileReader(filePath));) {

            boolean skip = true;

            while ((line=reader.readLine())!=null){
                if(skip){
                    skip=false;
                    continue;
                }

                String[] data = line.split(",");

                CsvFileContent csvFileContent = new CsvFileContent();
                csvFileContent.setSourceCurrency(data[0]);
                csvFileContent.setTargetCurrency(data[1]);
                csvFileContent.setRate(data[2]);
                csvFileContent.setLastColumn(null);

                contents.add(csvFileContent);
            }

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }catch (IOException e){
            e.printStackTrace();
        }

//        log.info("data -> {}", JsonConverter.toJson(contents, true));
        return contents;
    }
}
